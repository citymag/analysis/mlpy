<img src="https://mloss.org/media/screenshot_archive/mlpy_logo.png" alt="mlpy Logo" width="35%" />
<br /><br />


`mlpy` is a Python module for **Machine Learning** built on top of 
[NumPy/SciPy](http://www.scipy.org/) and the 
[GNU Scientific Libraries](http://www.gnu.org/s/gsl/).

mlpy provides a wide range of state-of-the-art machine learning methods for 
**supervised** and **unsupervised** problems and it is aimed at finding a 
reasonable compromise among modularity, maintainability, reproducibility, 
usability and efficiency. 

mlpy is **multiplatform**, it works with Python 2 and 3 and it is Open Source, 
distributed under the GNU General Public License version 3.

### if you use mlpy, please cite:

D. Albanese, R. Visintainer, S. Merler, S. Riccadonna, G. Jurman, C. Furlanello. mlpy: Machine Learning Python, 2012.
[arXiv:1202.6548](http://arxiv.org/abs/1202.6548) [[bib]](http://mlpy.sourceforge.net/mlpy.bib)

<p style="text-align: center">
	<img src="http://mlpy.sourceforge.net/images/screenshots.jpg" alt="screenshots">
</p>

## Installation

### New (recommended) method

After fixing an issue regarding the missed location of the GSL libraries in Macports, it
is now possible to install this slightly modified package using `pip` directly:

```
sudo pip install -I git+https://gitlab.com/citymag/analysis/mlpy.git
```

### Former way (manually providing GSL path)

The first time we tried installing the package, the following error message could be seen:

```
mlpy/gsl/gsl.c:223:10: fatal error: 'gsl/gsl_sf.h' file not found
#include <gsl/gsl_sf.h>
         ^~~~~~~~~~~~~~
```

This occurred because the correct path to the GSL libraries (which are installed using the
Macports packag manager) was not provided in the `setup.py`. The first workaround that was
used was to clone the repository and run the `setup.py` using some additional arguments to
specify the right path from the Macports directory: 

```
sudo python setup.py build_ext --include-dirs=/opt/local/include --rpath=/opt/local/lib
sudo python setup.py install
```

Fortunately, following the little changes found in [this version](https://06128180587070612406.googlegroups.com/attach/3f9d7f06dcab96b4/setup.py?part=0.1&view=1&vt=ANaJVrEmTC2q1ZL0lNRfrZSFlUpORAJOrxJGRNo3Fv8PU7UCXNIHYQVITumulxuBjKoKYTvV5Wp7ExQPEw2Gaw8Fa9IzQmgHyrXWrjW20PSfpV_cIrwMp24) of the `setup.py` file for that package (see the variables `mp_libdir` and `mp_includedir`), we were able to build a workable setup file, therefore allowing us to install the package directly using `pip` and without the need to clone the repository locally (see command in the precedent section).


### Old methods (from original readme file)

It is possible to download and install `mlpy` directly from this repository. In particular, this repo
provides pre-compiled Python eggs for Linux (`x86_64`) and OSX platforms, available
for both Python 2.7 and Python 3.5:

|           | **Linux** `x86_64`                                                                 |  **OSX** `macosx`                                                                        |
|-----------|------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
| **Py2.7** | https://gitlab.fbk.eu/MPBA/mlpy/blob/master/dist/mlpy-3.5.0-py2.7-linux-x86_64.egg | https://gitlab.fbk.eu/MPBA/mlpy/raw/master/dist/mlpy-3.5.0-py2.7-macosx-10.9-x86_64.egg  |
| **Py3.5** | https://gitlab.fbk.eu/MPBA/mlpy/blob/master/dist/mlpy-3.5.0-py3.5-linux-x86_64.egg | https://gitlab.fbk.eu/MPBA/mlpy/blob/master/dist/mlpy-3.5.0-py3.5-macosx-10.9-x86_64.egg |


#### Installing Python egg via `pip`:

```
pip install -e git+https://gitlab.fbk.eu/MPBA/mlpy.git#egg=mlpy
```

>**Note:** The same constraints on matching platforms and Python version **must apply** to install Python eggs.
>The very difference is in the fact that `pip` will perform the `(python version, platform)` matching
>automagically for you, depending on your current python version and operating system.

#### Installing from source via `pip`:

```
pip install git+https://gitlab.fbk.eu/MPBA/mlpy.git
```

>**Note:** This will download, compile and install `mlpy` from source.
>Please make sure to satisfy all the requirements, namely _a working `gcc` compiler_ and the latest _numpy_ and
>_gsl_ libraries installed.

#### Importing package from egg file

In order to import the mlpy package in the script, the following can be done:

```
import sys
sys.path = ['/usr/local/lib/python3.6/dist-packages/mlpy-3.5.0-py3.6-linux-x86_64.egg'] + sys.path
```

## Bug fixing

In this version, a bug was fixed to resolved the following error:

```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-12-c694be8171a2> in <module>
      2 import matplotlib.pyplot as plt
      3 omega0=6
----> 4 scales = mlpy.wavelet.autoscales(N=len(data),dt=1,dj=0.05,wf='morlet',p=omega0)
      5 spec = mlpy.wavelet.cwt(data[:,1],dt=1,scales=scales,wf='morlet',p=omega0)
      6 freq = (omega0 + numpy.sqrt(2.0 + omega0 ** 2)) / (4 * numpy.pi * scales[1:])

/opt/local/Library/Frameworks/Python.framework/Versions/3.6/lib/python3.6/site-packages/mlpy/wavelet/continuous.py in autoscales(N, dt, dj, wf, p)
    160 
    161      J = floor(dj**-1 * log2((N * dt) / s0))
--> 162      s = empty(J + 1)
    163 
    164      for i in range(s.shape[0]):

TypeError: 'numpy.float64' object cannot be interpreted as an integer
```

The fix consisted of changing line 162 from the above error message to the following:

``` python
s = empty(int(J) + 1)
```

## Features

**Regression**: Least Squares, Ridge Regression, Last Angle Regression, Elastic Net, Kernel Ridge Regression, 
Support Vector Machines (SVR), Partial Least Squares (PLS)

**Classification**: Linear Discriminant Analysis (LDA), Basic Perceptron, Elastic Net, Logistic Regression, 
(Kernel) Support Vector Machines (SVM), Diagonal Linear Discriminant Analysis (DLDA), Golub Classifier, 
Parzen-based, (kernel) Fisher Discriminant Classifier k-Nearest-Neighbor, Iterative RELIEF, 
Classification Tree, Maximum Likelihood Classifier

**Clustering**: Hierarchical Clustering, Memory-saving Hierarchical Clustering, k-means

**Dimensionality Reduction**: (Kernel) Fisher Discriminant (FDA), Spectral Regression Discriminant Analysis (SRDA),
 (kernel) Principal Component Analysis (PCA)

**Wavelet Submodule** (`mlpy.wavelet`): Discrete, Undecimated and Continuous Wavelet Transform

**Misc**: Feature ranking/selection algorithms, Canberra stability indicator, resampling algorithms, error evaluation, 
peak finding algorithms, Dynamic Time Warping (DTW) distance, Longest Common Subsequence (LCS).


## Documentation

The latest online documentation is available [here](http://mlpy.sourceforge.net/docs/)

## People

* **Lead Developer**: Davide Albanese
* **Contributors**: Giuseppe Jurman, Stefano Merler, Roberto Visintainer, Marco Chierici, Lance Hepler.

### Financial Contributors

<p>
	<img src="http://mlpy.sourceforge.net/images/fbk_logo.jpg" align="left" alt="FBK logo">
	<img src="http://mlpy.sourceforge.net/images/airc_logo.gif" align="left" hspace="8" alt="AIRC logo"> mlpy is a
	project of <a href="http://mpba.fbk.eu">Predictive Models for
	Biological Predictive Models for Biological and Environmental
	Data Analysis (MPBA)</a> Research Unit
	at <a href="http://www.fbk.eu">Fondazione Bruno Kessler</a>
	and it is co-funded
	by <a href="http://www.airc.it/">Associazione Italiana per la
	Ricerca sul Cancro</a>
</p>

